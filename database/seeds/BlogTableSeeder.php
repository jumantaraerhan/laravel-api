<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error voluptate recusandae iure eum est, facilis dolorum, 
            modi quaerat sequi assumenda incidunt ipsam consectetur natus numquam voluptatibus similique, unde dolore temporibus.',
            'category' => 'LOREM RI 12'
        ]);
    }
}
